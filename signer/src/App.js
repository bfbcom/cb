import React, { Component } from 'react';
import crypto from 'crypto'
import KeyManagement from './KeyManagement'
import SignIn from './SignIn'
import db from './DB'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      password: null
    }

    document.addEventListener("pause", this.signOut, false)
    document.addEventListener("resume", this.signOut, false)
    document.addEventListener("menubutton", this.signOut, false)
  }

  signOut() {
    this.setState({ password: null })
  }

  onSignIn(password) {
    this.setState({ password: password })
  }

  render() {
    if (this.state.password) {
      return (
        <KeyManagement password={this.state.password} />
      )
    } else {
      return (
        <SignIn onSignIn={this.onSignIn.bind(this)} />
      )
    }

  }
}

export default App;
