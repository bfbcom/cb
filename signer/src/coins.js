const btc = {
  name: 'Bitcoin',
  symbol: 'BTC',
  icon: './icons/BTC.svg',
  networks: {
    test: {
      messagePrefix: '\x18Bitcoin Signed Message:\n',
      name: 'test',
      bip32: {
        // private: 0x04358394,
        // public: 0x043587cf
        public: 0x0488b21e,
        private: 0x0488ade4
      },
      bip44: 1,
      private: 0xef,
      public: 0x6f,
      scripthash: 0xc4,
      scriptHash: 0xc4,
      pubKeyHash: 0x6f,
      wif: 0xef,
      txUrl: "https://test-insight.bitpay.com/tx/{{txId}}",
      api: {
        addressBalance: "https://test-insight.bitpay.com/api/addr/{{address}}",
        addressTxs: "https://test-insight.bitpay.com/api/txs/?address={{address}}",
        addressUtxo: "https://test-insight.bitpay.com/api/addr/{{address}}/utxo",
        broadcastTx: "https://test-insight.bitpay.com/api/tx/send",
        fee: "https://test-insight.bitpay.com/api/utils/estimatefee"
      }
    },
    main: {
      name: 'main',
      versions: {
        bip32: {
          private: 0x0488ade4,
          public: 0x0488b21e
        },
        bip44: 0,
        private: 0x80,
        public: 0x00,
        scripthash: 0x05
      },
      txUrl: "https://insight.bitpay.com/tx/{{txId}}",
      api: {
        addressBalance: "https://insight.bitpay.com/api/addr/{{address}}",
        addressTxs: "https://insight.bitpay.com/api/txs/?address={{address}}",
        broadcastTx: "https://insight.bitpay.com/api/tx/send",
      }
    }
  }
}

const  ltc = {
  name: 'Litecoin',
  symbol: 'LTC',
  icon: './icons/LTC.svg',
  networks: {
    main: {
      versions: {
        bip32: {
          // private: 0x019d9cfe,
          // public: 0x019da462
          public: 0x0488b21e,
          private: 0x0488ade4
        },
        bip44: 2,
        private: 0xb0,
        public: 0x30,
        scripthash: 0x32,
        scripthash: 0x32,
        scripthash2: 0x05
      },
      txUrl: "https://insight.bitpay.com/tx/{{txId}}"
    },
    test: {
      messagePrefix: '\x18Litecoin Signed Message:\n',
      name: 'test',
      bip32: {
        // private: 0x04358394,
        // public: 0x043587cf
        // private: 0x04358394,
        // public: 0x043587cf
        public: 0x0488b21e,
        private: 0x0488ade4
      },
      bip44: 1,
      private: 0xef,
      public: 0x6f,
      scripthash: 0x3a,
      scripthash2: 0xc4,
      pubKeyHash: 0x6f,
      scriptHash: 0x3a,
      wif: 0xef,
      txUrl: "http://35.184.37.137:3000/tx/{{txId}}",
      api: {
        addressBalance: "http://35.184.37.137:3000/insight-lite-api/addr/{{address}}",
        addressTxs: "http://35.184.37.137:3000/insight-lite-api/txs/?address={{address}}",
        addressUtxo: "http://35.184.37.137:3000/insight-lite-api/addr/{{address}}/utxo",
        broadcastTx: "http://35.184.37.137:3000/insight-lite-api/tx/send",
        fee: "http://35.184.37.137:3000/insight-lite-api/utils/estimatefee"
      }


      //
      // versions: {
      //   bip44: 1,
      //   private: 0xef,
      //   public: 0x6f,
      //   scripthash: 0x3a,
      //   scripthash2: 0xc4
      // },
      // txUrl: "https://test-insight.bitpay.com/tx/{{txId}}"
    }
  }
}

const dash = {
  name: 'Dash',
  symbol: 'DASH',
  icon: './icons/DASH.svg',
  networks: {
    main: {
      messagePrefix: '\x18Dash Signed Message:\n',
      name: 'main',
      bip32: {
        // private: 0x04358394,
        // public: 0x043587cf
        public: 0x0488b21e,
        private: 0x0488ade4
      },
      bip44: 5,
      private: 0xcc,
      public: 0x4c,
      scripthash: 0x10,
      scriptHash: 0x10,
      pubKeyHash: 0x4c,
      wif: 0xef,
      txUrl: "https://insight.dash.org/insight/tx/{{txId}}",
      api: {
        addressBalance: "https://insight.dash.org/api/addr/{{address}}",
        addressTxs: "https://insight.dash.org/api/txs/?address={{address}}",
        addressUtxo: "https://insight.dash.org/api/addr/{{address}}/utxo",
        broadcastTx: "https://insight.dash.org/api/tx/send",
        fee: "https://insight.dash.org/api/utils/estimatefee"
      }
    }
  }
}
//
// const doge = {
//   name: 'Dogecoin',
//   symbol: 'DOGE',
//   icon: './icons/DOGE.svg',
//   networks: {
//     test: {
//       versions: {
//         bip44: 1,
//         private: 0xf1,
//         public: 0x71,
//         scripthash: 0xc4
//       },
//       txUrl: "https://test-insight.bitpay.com/tx/{{txId}}"
//     },
//     main: {
//       versions: {
//         bip32: {
//           private: 0x02fac398,
//           public: 0x02facafd
//         },
//         bip44: 3,
//         private: 0x9e,
//         public: 0x1e,
//         scripthash: 0x16
//       },
//       txUrl: "https://insight.bitpay.com/tx/{{txId}}"
//     }
//   }
// }
//
// const eth = {
//   name: 'Ethereum',
//   symbol: 'ETH',
//   icon: './icons/ETH.svg',
//   networks: {
//     main: {
//       providerUri: "http://52.208.46.161:8546",
//       txUrl: "https://etherscan.io/tx/{{txId}}"
//     },
//     ropsten: {
//       providerUri: "http://52.208.46.161:8549",
//       txUrl: "https://ropsten.etherscan.io/tx/{{txId}}"
//     }
//   }
// }

// export { btc, dash, doge, eth, ltc }

const  dcr = {
  name: 'Decred',
  symbol: 'DCR',
  icon: './icons/DCR.svg',
  networks: {
    test: {
      messagePrefix: '\x18Decred Signed Message:\n',
      name: 'test',
      bip32: {
        public: 0x0488b21e,
        private: 0x0488ade4
      },
      bip44: 42,
      private: 0x230e,
      public: 0x0f21,
      pubKeyHash: 0x0f21,
      scripthash: 0x0efc,
      scriptHash: 0x0efc,
      wif: 0xef,
      txUrl: "https://testnet.decred.org/tx/{{txId}}",
      api: {
        addressBalance: "https://testnet.decred.org/api/addr/{{address}}",
        addressTxs: "https://testnet.decred.org/api/txs/?address={{address}}",
        addressUtxo: "https://testnet.decred.org/api/addr/{{address}}/utxo",
        broadcastTx: "https://testnet.decred.org/api/tx/send",
        fee: "https://testnet.decred.org/api/utils/estimatefee"
      }


      //
      // versions: {
      //   bip44: 1,
      //   private: 0xef,
      //   public: 0x6f,
      //   scripthash: 0x3a,
      //   scripthash2: 0xc4
      // },
      // txUrl: "https://test-insight.bitpay.com/tx/{{txId}}"
    }
  }
}

const  qtum = {
  name: 'Qtum',
  symbol: 'QTUM',
  icon: './icons/QTUM.png',
  networks: {
    main: {
      messagePrefix: '\x18Qtum Signed Message:\n',
      name: 'main',
      bip32: {
        public: 0x0488b21e,
        private: 0x0488ade4
      },
      bip44: 2301,
      private: 0x80,
      public: 0x3A,
      scripthash: 0x32,
      scriptHash: 0x32,
      pubKeyHash: 0x3A,
      wif: 0xef,
      txUrl: "https://explorer.qtum.org/tx/{{txId}}",
      api: {
        addressBalance: "https://explorer.qtum.org/insight-api/addr/{{address}}",
        addressTxs: "https://explorer.qtum.org/insight-api/txs/?address={{address}}",
        addressUtxo: "https://explorer.qtum.org/insight-api/addr/{{address}}/utxo",
        broadcastTx: "https://explorer.qtum.org/insight-api/tx/send",
        fee: "https://explorer.qtum.org/insight-api/utils/estimatefee"
      }
    },
    test: {
      messagePrefix: '\x18Qtum Signed Message:\n',
      name: 'test',
      bip32: {
        public: 0x0488b21e,
        private: 0x0488ade4
      },
      bip44: 2301,
      private: 0xef,
      public: 0x78,
      scripthash: 0x6e,
      scriptHash: 0x6e,
      pubKeyHash: 0x78,
      wif: 0xef,
      txUrl: "https://testnet.qtum.org/tx/{{txId}}",
      api: {
        addressBalance: "https://testnet.qtum.org/insight-api/addr/{{address}}",
        addressTxs: "https://testnet.qtum.org/insight-api/txs/?address={{address}}",
        addressUtxo: "https://testnet.qtum.org/insight-api/addr/{{address}}/utxo",
        broadcastTx: "https://testnet.qtum.org/insight-api/tx/send",
        fee: "https://testnet.qtum.org/insight-api/utils/estimatefee"
      }
    }
  }
}


const vtc = {
  name: 'Vertcoin',
  symbol: 'VTC',
  icon: './icons/VTC.png',
  networks: {
    main: {
      messagePrefix: '\x18Vertcoin Signed Message:\n',
      name: 'main',
      bip32: {
        private: 0x0488ade4,
        public: 0x0488b21e
      },
      bip44: 28,
      private: 0x80,
      public: 0x47,
      scripthash: 0x05,
      scriptHash: 0x05,
      pubKeyHash: 0x47,
      wif: 0xef,
      txUrl: "https://insight.vertcoin.org/tx/{{txId}}",
      api: {
        addressBalance: "https://insight.vertcoin.org/insight-vtc-api/addr/{{address}}",
        addressTxs: "https://insight.vertcoin.org/insight-vtc-api/txs/?address={{address}}",
        addressUtxo: "https://insight.vertcoin.org/insight-vtc-api/addr/{{address}}/utxo",
        broadcastTx: "https://insight.vertcoin.org/insight-vtc-api/tx/send",
        fee: "https://insight.vertcoin.org/insight-vtc-api/utils/estimatefee"
      }
    }
  }
}

const grs = {
  name: 'Groestlcoin',
  symbol: 'GRS',
  icon: './icons/GRS.svg',
  networks: {
    main: {
      messagePrefix: '\x18Groestlcoin Signed Message:\n',
      name: 'main',
      bip32: {
        private: 0x0488ade4,
        public: 0x0488b21e
      },
      bip44: 28,
      private: 0x80,
      public: 0x24,
      scripthash: 0x05,
      scriptHash: 0x05,
      pubKeyHash: 0x24,
      wif: 0xef,
      txUrl: "https://groestlsight.groestlcoin.org/tx/{{txId}}",
      api: {
        addressBalance: "https://groestlsight.groestlcoin.org/api/addr/{{address}}",
        addressTxs: "https://groestlsight.groestlcoin.org/api/txs/?address={{address}}",
        addressUtxo: "https://groestlsight.groestlcoin.org/api/addr/{{address}}/utxo",
        broadcastTx: "https://groestlsight.groestlcoin.org/api/tx/send",
        fee: "https://groestlsight.groestlcoin.org/api/utils/estimatefee"
      }
    }
  }
}

const mona = {
  name: 'Mona',
  symbol: 'MONA',
  icon: './icons/MONA.svg',
  networks: {
    main: {
      messagePrefix: '\x18Mona Signed Message:\n',
      name: 'main',
      bip32: {
        private: 0x0488ade4,
        public: 0x0488b21e
      },
      bip44: 22,
      private: 0xb0,
      private2: 0xb2, // old wif
      public: 0x32,
      scripthash: 0x37,
      scriptHash: 0x37,
      scripthash2: 0x05,
      pubKeyHash: 0x32,
      wif: 0xef,
      txUrl: "https://mona.chainsight.info/tx/{{txId}}",
      api: {
        addressBalance: "https://mona.chainsight.info/api/addr/{{address}}",
        addressTxs: "https://mona.chainsight.info/api/txs/?address={{address}}",
        addressUtxo: "https://mona.chainsight.info/api/addr/{{address}}/utxo",
        broadcastTx: "https://mona.chainsight.info/api/tx/send",
        fee: ""
      }
    }
  }
}

export { btc, dash, grs, ltc, qtum, vtc }
