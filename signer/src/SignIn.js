import React, { Component } from 'react'
import crypto from 'crypto'
import { Page, Toast, Input, Button } from 'react-onsenui'
import Secret from './Secret'
import Loading from './Loading'

class SignIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      password: null,
      toastShown: false,
      messageError: null,
      loading: false,
      initialized: window.localStorage.getItem('initialized')
    }
  }

  toggleLoading() {
    this.setState({ loading: !this.state.loading })
  }

  confirm() {
    const passwordSecret = crypto.randomBytes(32).toString('hex')
    const pw = Secret.createPasswordKey(this.state.password)
    const encrypted = Secret.encryptWithPassword(passwordSecret, pw)
    window.localStorage.setItem('initialized', encrypted)
    this.setState({ initialized: encrypted })
    this.props.onSignIn(pw)
  }

  signIn() {
    try {
      const pw = Secret.createPasswordKey(this.state.password)
      Secret.decryptWithPassword(this.state.initialized, pw)
      this.props.onSignIn(pw)
    } catch(e) {
      console.log('error', e)
      this.setState({ toastShown: true, messageError: 'Invalid password'})
      setTimeout(() => {
        this.setState({ toastShown: false })
      }, 4000);
    }
  }

  handleInputChange(event) {
    this.setState({ [event.target.name]: event.target.value })
  }

  render() {
    if (this.state.initialized) {
      return (
        <Page>
          <div style={{padding: '20px', textAlign: 'center'}}>
            <h3>Welcome back</h3>
            <Input
              name='password'
              modifier='material'
              style={{width: '100%', textAlign: 'center'}}
              placeholder='Password'
              type='password'
              onChange={this.handleInputChange.bind(this)}
              value={this.state.password}
            />
            <br/><br/>
            <Button style={{width: '100%', textAlign: 'center'}} onClick={this.signIn.bind(this)}>
              Sign In
            </Button>
            <Toast isOpen={this.state.toastShown}>
              <div className="message">
                {this.state.messageError}
              </div>
            </Toast>
          </div>
          <Loading show={this.state.loading} />
        </Page>
      )
    } else {
      return (
        <Page>
          <div style={{padding: '20px', textAlign: 'center'}}>
            <h3>Welcome</h3>
            <p>
              Define a password to protect your keys.
            </p>
            <Input
              name='password'
              modifier='material'
              style={{width: '100%', textAlign: 'center'}}
              placeholder='Password'
              type='password'
              onChange={this.handleInputChange.bind(this)}
              value={this.state.password}
            />
            <Button style={{width: '100%', textAlign: 'center'}} onClick={this.confirm.bind(this)}>
              Confirm
            </Button>
            <Toast isOpen={this.state.toastShown}>
              <div className="message">
                {this.state.messageError}
              </div>
            </Toast>
            <Loading show={this.state.loading} />
          </div>
        </Page>
      )
    }
  }

}

export default SignIn;
