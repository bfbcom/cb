import React, { Component } from 'react';
import { Button } from 'react-onsenui'

export default class Scanner extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    console.log('show scanner', this.props.show)
    const qrcode = {
      marginLeft: 'auto',
      marginRight: 'auto',
      display: 'table',
      border: '4px solid #DDD',
      width: 220,
      height: 220,
      marginTop: 140,
    }

    if (this.props.show) {
      return (
        <div style={{background: 'transparent', zIndex: 99999, width: '100%', height: '100%', position: 'absolute'}}>
          <div style={qrcode}></div>
          <div style={{width: '100%', margin: '0 auto', display: 'block', textAlign: 'center', position: 'absolute', bottom: '10px'}}>
            <Button onClick={this.props.onCancel}>Cancel</Button>
          </div>
        </div>
      )
    } else {
      return null
    }
  }
}
