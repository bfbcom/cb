import React, { Component } from 'react';
import crypto from 'crypto'
import { Modal, Icon } from 'react-onsenui'

export default class Loading extends Component {
  render() {
    return (
      <Modal isOpen={this.props.show}>
        <section style={{margin: 16}}>
          <Icon style={{fontSize: 40, color: '#DDD'}} spin icon='md-spinner' />
        </section>
      </Modal>
    )
  }
}
