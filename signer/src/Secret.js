import crypto from 'crypto'

const browserFingerprint = (w) => {
  let nav = w.navigator
  let screen = w.screen
  let id = nav.mimeTypes.length
  id += nav.userAgent.replace(/\D+/g, '')
  id += nav.plugins.length
  id += screen.height || ''
  id += screen.width || ''
  id += screen.pixelDepth || ''

  return id
}

const createPasswordKey = (passphrase) => {
  const masterSalt = "96a81fb1-9127-40++CoinBundlerPassword++e0-aace-03884fa3f8b9"
  const salt = crypto.createHmac('sha512', passphrase).update(masterSalt).digest('hex')
  return crypto.pbkdf2Sync(passphrase,
    salt, 20000, 64, 'sha512')
}

const encryptWithPassword = (message, password) => {
  const cipher = crypto.createCipher('aes256', password)
  let encrypted = cipher.update(message, 'utf8', 'hex')
  encrypted += cipher.final('hex')

  return encrypted
}

const decryptWithPassword = (encrypted, password) => {
  const decipher = crypto.createDecipher('aes256', password);

  let decrypted = decipher.update(encrypted, 'hex', 'utf8');
  decrypted += decipher.final('utf8');

  return decrypted
}

const Secret = {
  browserFingerprint: browserFingerprint,
  createPasswordKey: createPasswordKey,
  encryptWithPassword: encryptWithPassword,
  decryptWithPassword: decryptWithPassword
}

export default Secret
