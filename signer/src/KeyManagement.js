import React, { Component } from 'react';
import crypto from 'crypto'
import { Page, Dialog, Toast, Ripple, ProgressCircular, Input, Row, Col, Toolbar, ToolbarButton, Modal, Button, List, ListItem, ListHeader, Fab, Icon, ActionSheet, ActionSheetButton } from 'react-onsenui'
import ons from 'onsenui'
import db from './DB'
import bip39 from 'bip39'
import libcoin from 'bitcoinjs-lib'
import * as coinsConfig from './coins'
import Scanner from './Scanner'
import Secret from './Secret'
import Key from './Key'
import Blockchain from './Blockchain'
import Loading from './Loading'

const masterSalt = '6bcc3c61-bca5-4e++CoinBundlerKey++41-9e4c-7f3a77ea75bc'

class KeyManagement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      scanning: false,
      toastShown: false,
      toastMessage: null,
      loading: false,
      accountActionsIsOpen: false,
      keyActionsIsOpen: false,
      currentAccount: {
        alias: '',
        mnemonic: ''
      },
      currentAccount2: {},
      keyDialogIsOpen: false,
      settingsDialogIsOpen: false,
      txDialogIsOpen: false,
      seedModalIsOpen: false,
      seedProgress: 0,
      transaction: {
        to: ''
      }
    }
    this.loadAccounts()
    this.toggleKeyDialog = this.toggleKeyDialog.bind(this)
    this.toggleAccountActions = this.toggleAccountActions.bind(this)
  }

  toggleAccountActions() {
    this.setState({ accountActionsIsOpen: !this.state.accountActionsIsOpen })
  }

  toggleKeyActions() {
    this.setState({ keyActionsIsOpen: !this.state.keyActionsIsOpen })
  }

  toggleKeyDialog() {
    this.setState({ keyDialogIsOpen: !this.state.keyDialogIsOpen })
  }

  toggleTxDialog() {
    this.setState({ txDialogIsOpen: !this.state.txDialogIsOpen })
  }

  toggleLoading() {
    this.setState({ loading: !this.state.loading })
  }

  toggleSeedModal() {
    this.setState({ seedModalIsOpen: !this.state.seedModalIsOpen })
  }

  cancelScan() {
    this.setState({ scanning: false })
    window.QRScanner.hide()
    window.QRScanner.destroy()
  }

  signAction() {
    this.setState({ scanning: true })
    this.toggleAccountActions()
    window.QRScanner.scan((err, data) => {
      if(err){
        console.error(err._message);
      }
      data = JSON.parse(data)
      this.setState({ scanning: false })

      const network = coinsConfig[data.coin]['networks'][data.network]
      let key = new Key(this.state.currentAccount.seed, network)

      let tx = new libcoin.Transaction.fromHex(data.txraw)
      let txb = new libcoin.TransactionBuilder.fromTransaction(tx, network)
      txb.inputs.forEach((input, index) => {
        txb.sign(index, key._privateKey, Buffer.from(data.redeemScript, 'hex'))
      })

      this.setState({
        transaction: {
          amount: data.amount,
          to: data.to,
          coin: coinsConfig[data.coin].name,
          txhex: txb.build().toHex(),
          blockchain: new Blockchain(network)
        }
      })

      this.toggleTxDialog()


    })
    window.QRScanner.show(function(status){
      console.log(status)
      // page.style.opacity = '0.0'
    })
  }

  confirm() {
    this.toggleLoading()
    this.state.transaction.blockchain.broadcastTx(this.state.transaction.txhex).then(result => {
      this.toggleLoading()
      this.toggleTxDialog()
      this.setState({ toastMessage: 'Transaction broadcasted', toastShown: true })
      setTimeout(() => {
        this.setState({ toastShown: false })
      }, 4000);
    }).catch(err => {
      this.toggleLoading()
      this.toggleTxDialog()
      this.setState({ toastMessage: 'Something went wrong', toastShown: true })
      setTimeout(() => {
        this.setState({ toastShown: false })
      }, 4000);
    });
  }

  deleteAction() {
    ons.notification.confirm('CAUTION! This key will be deleted and you only can recover it using the backup phrase. Are you sure?', { modifier: 'material' }).then((response) => {
      if (response) {
        let _this = this
        db.Accounts.delete(this.state.currentAccount.id).then(result => {
          console.log('delete', result)
          _this.toggleAccountActions()
          _this.loadAccounts()
        })
      }
    })
  }

  showSecretsAction() {
    this.toggleAccountActions()
    this.toggleKeyDialog()
  }

  createKeyAction() {
    this.seed = new Date().toString()
    this.toggleKeyActions()
    if (this.state.seedProgress < 100) {
      this.toggleSeedModal()
    } else {
      this.toggleKeyDialog()
    }
  }

  restoreKeyAction() {
    this.setState({ currentAccount: { alias: '', mnemonic: '', pubkey: null, secrets: null } })
    this.toggleKeyActions()
    this.toggleKeyDialog()
  }

  copyPublicKey() {
    if (window.cordova)
      window.cordova.plugins.clipboard.copy(this.state.currentAccount.pubkey)
    else
      console.log(this.state.currentAccount.pubkey)
  }

  _buildMnemonic(seed) {
    const mnemonic = bip39.entropyToMnemonic(seed)
    let hdkey = libcoin.HDNode.fromSeedHex(seed, coinsConfig.btc.networks.test).derivePath("m/46'")
    return { mnemonic: mnemonic, pubkey: hdkey.neutered().toBase58() }
  }

  save() {
    let _this = this
    let account = { alias: this.state.currentAccount.alias }
    if (this.state.currentAccount.secrets) {
      account.secrets = this.state.currentAccount.secrets
    } else {
      if (bip39.validateMnemonic(this.state.currentAccount.mnemonic)) {
        const pvk = bip39.mnemonicToEntropy(this.state.currentAccount.mnemonic)
        account.secrets = pvk
      } else {
        this.setState({ toastShown: true, toastMessage: 'Invalid mnemonic' })
        setTimeout(() => {
          this.setState({ toastShown: false })
        }, 4000);
        return
      }

    }
    db.Accounts.create({ alias: this.state.currentAccount.alias, secrets: Secret.encryptWithPassword(account.secrets, this.props.password)}).then(result => {
      _this.setState({ seedProgress: 0, currentAccount: Object.assign(this.state.currentAccount, {id: result[0].insertId})})
      _this.loadAccounts()
    })
  }

  loadAccounts() {
    db.Accounts.all().then(result => {
      let rows = db.aggregateRows(result)
      this.setState({ accounts: rows })
    })
  }

  selectAccount(account) {
    let seed = Secret.decryptWithPassword(account.secrets, this.props.password)
    let { mnemonic: mnemonic, pubkey: pubkey } = this._buildMnemonic(seed)
    this.setState({currentAccount: Object.assign(account, { seed: seed, mnemonic: mnemonic, pubkey: pubkey })})
    this.toggleAccountActions()
  }

  handleInputChange(event) {
    this.setState({ currentAccount: Object.assign(this.state.currentAccount, { [event.target.name]: event.target.value }) })
  }

  entropy(event) {
    if (this.state.seedProgress < 100) {
      this.state.seedProgress++
      this.seed += event.clientX + event.clientY + new Date().getTime()
    } else {
      this.toggleSeedModal()
      this.toggleLoading()

      const salt = crypto.createHmac('sha512', masterSalt).update(Secret.browserFingerprint(window) + new Date().toString()).digest('hex')
      crypto.pbkdf2(this.seed, salt, 100000, 16, 'sha512', (err, derivedKey) => {
        if (err) throw err;
        let { mnemonic: mnemonic, pubkey: pubkey } = this._buildMnemonic(derivedKey.toString('hex'))
        this.setState({ currentAccount: { alias: '', mnemonic: mnemonic, pubkey: pubkey, secrets: derivedKey.toString('hex') } })
        this.toggleLoading()
        this.toggleKeyDialog()
      })

    }
  }

  render() {
    return (
      <div>
        <Page modifier='material' style={{ display: this.state.scanning ? 'none' : 'initial'}} renderToolbar={() => <Toolbar> <div className='center'>My Keys</div> <div className='right'> <ToolbarButton>  </ToolbarButton> </div> </Toolbar> }>
          <List
            dataSource={this.state.accounts}
            renderRow={(acc) => <ListItem onClick={() => {this.selectAccount(acc)}}>{acc.alias}</ListItem>}
          />

          <Fab
            onClick={this.toggleKeyActions.bind(this)}
            position='bottom right'>
            <Icon icon='md-plus' />
          </Fab>

          <Modal isOpen={this.state.seedModalIsOpen} >
            <section style={{
              width: '100%',
              height: '100%',
              margin: '0 auto',
              backgroundColor: 'grey',
            }} onClick={this.entropy.bind(this)}>
              <div style={{
                marginTop: '-100px',
                marginLeft: '-100px',
                position: 'fixed',
                top: '50%',
                left: '50%',
                width: '200px',
                height: '100px',
                color: '#DDD',
                fontSize: '20px'
              }}>
                Tap! Tap! Tap! Tap!
              </div>
              <Ripple color='rgba(40, 30, 70, 0.4)' background='rgba(125, 125, 125, 0.4)' />
            </section>
          </Modal>

          <ActionSheet isOpen={this.state.accountActionsIsOpen} animation='default'
            onCancel={this.toggleAccountActions.bind(this)}
            isCancelable={true}
            modifier='material'
          >
            <ActionSheetButton modifier='material' onClick={this.signAction.bind(this)}>Sign</ActionSheetButton>
            <ActionSheetButton modifier='material' onClick={this.showSecretsAction.bind(this)}>Show secrets</ActionSheetButton>
            <ActionSheetButton modifier='material' onClick={this.deleteAction.bind(this)}>Delete</ActionSheetButton>
            <ActionSheetButton modifier='material' onClick={this.toggleAccountActions.bind(this)} className='cancel-action-sheet-button'>Cancel</ActionSheetButton>
          </ActionSheet>

          <ActionSheet isOpen={this.state.keyActionsIsOpen} animation='default'
            onCancel={this.toggleKeyActions.bind(this)}
            isCancelable={true}
            modifier='material'
          >
            <ActionSheetButton modifier='material' onClick={this.createKeyAction.bind(this)}>Create new key</ActionSheetButton>
            <ActionSheetButton modifier='material' onClick={this.restoreKeyAction.bind(this)}>Restore key</ActionSheetButton>
            <ActionSheetButton modifier='material' onClick={this.toggleKeyActions.bind(this)} className='cancel-action-sheet-button'>Cancel</ActionSheetButton>
          </ActionSheet>

          <Dialog
            isOpen={this.state.keyDialogIsOpen}
            isCancelable={true}
            onCancel={this.toggleKeyDialog.bind(this)}>
            <div style={{textAlign: 'center', margin: '20px'}}>
              <Input
                name='alias'
                modifier='material'
                style={{width: '100%', textAlign: 'center'}}
                placeholder='Account alias'
                value={this.state.currentAccount.alias}
                onChange={this.handleInputChange.bind(this)}
              />
              <textarea style={{width: '100%'}} name='mnemonic' onChange={this.handleInputChange.bind(this)} value={this.state.currentAccount.mnemonic}></textarea>
              <div>
                <Button style={{width: '50%'}} disabled={this.state.currentAccount.id} onClick={this.save.bind(this)}>Save</Button>
                <Button style={{width: '50%'}} disabled={!this.state.currentAccount.id} onClick={this.copyPublicKey.bind(this)}>Copy public key</Button>
              </div>
              <p>
                <Button style={{width: '100%'}} modifier='quiet' onClick={this.toggleKeyDialog.bind(this)}>Close</Button>
              </p>
            </div>
          </Dialog>

          <Dialog
            isOpen={this.state.txDialogIsOpen}
            isCancelable={true}
            onCancel={this.toggleTxDialog.bind(this)}>
            <div style={{textAlign: 'left', margin: '20px'}}>
              <h5>{this.state.transaction.coin}</h5>
              <ul className='transaction-details'>
                <li><span>Amount:</span> {this.state.transaction.amount}</li>
                <li><span>To:</span> {this.state.transaction.to}</li>
              </ul>

              <p style={{marginTop: 10}}>
                <Button style={{width: '50%'}} modifier='quiet' onClick={this.toggleTxDialog.bind(this)}>Cancel</Button>
                <Button style={{width: '50%'}} modifier='material' onClick={this.confirm.bind(this)}>Confirm</Button>
              </p>
            </div>
          </Dialog>

          <Toast isOpen={this.state.toastShown}>
            <div className="message">
              {this.state.toastMessage}
            </div>
          </Toast>
        </Page>
        <Scanner show={this.state.scanning} onCancel={this.cancelScan.bind(this)} />
        <Loading show={this.state.loading} />
      </div>
    )
  }
}

export default KeyManagement;
