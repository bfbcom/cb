import websql from 'websql-promisified';

const database = openDatabase('wallet_db', '1.0', 'Wallet Database', 2 * 1024 * 1024);
const websqlPromise = websql(database);

const createSchema = () => {
  return websqlPromise.transaction((tx) => {
    tx.executeSql('CREATE TABLE IF NOT EXISTS accounts (alias, secrets)')
  })
}

const aggregateRows = (result) => {
  let records = []
  for(let i=0; i< result[0].rows.length; i++) {
      let row = result[0].rows.item(i)
      records.push(row)
   }

   return records
}

const test = () => {
  console.log("TEST LOAD DB")
}

const db = {
  insert: (table, object) =>
    websqlPromise.transaction((tx) => {
      tx.executeSql("INSERT INTO " + table + " (" + Object.keys(object).join(",") + ") VALUES ('" + Object.values(object).join("','") + "')")
    }),
  find: (table, id) =>
    websqlPromise.transaction((tx) => {
      tx.executeSql("SELECT rowid AS id,* FROM " + table + " WHERE rowid = '" + id + "'")
    }),
  delete: (table, id) =>
    websqlPromise.transaction((tx) => {
      tx.executeSql("DELETE FROM " + table + " WHERE rowid = '" + id + "'")
    }),
  where: (table, conditions) =>
    websqlPromise.transaction((tx) => {
      console.log("CONDITIONS", conditions)
      var query = conditions
      if (conditions && typeof(conditions) === 'object') {
        var numColumns = Object.keys(conditions).length,
            addedColumns = 0

        query = ""

        for (let [key, value] of Object.entries(conditions)) {
          addedColumns++
          query += key + " = '" + value + "'"
          if (addedColumns < numColumns) query += " AND "
        }
      } else {
        query = "1 = 1"
      }
      console.log('query', query)
      tx.executeSql("SELECT rowid AS id,* FROM " + table + " WHERE " + query)
    })
};

const Accounts = {
  create: account =>
    db.insert('accounts', account),
  where: conditions =>
    db.where('accounts', conditions),
  all: () =>
    db.where('accounts'),
  delete: id =>
    db.delete('accounts', id)
}

export default {
  Accounts,
  createSchema,
  aggregateRows,
};
