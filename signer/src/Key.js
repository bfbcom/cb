import libcoin from 'bitcoinjs-lib'

export default class Key {
  constructor(seed, network) {
    this._hdkey = libcoin.HDNode.fromSeedHex(seed, network)
    this._privateKey = this._hdkey.derivePath("m/46'/" + network.bip44 + "/0/0").keyPair
  }
}
