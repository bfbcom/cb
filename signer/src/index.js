import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import db from './DB'
import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';
import './index.css';

db.createSchema()

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
