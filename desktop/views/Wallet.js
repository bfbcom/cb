import io from 'socket.io-client';
import withApp from '../withApp'
import React, { Component } from 'react';
import * as coinsConfig from '../coins';
import WalletFactory from '../coins/WalletFactory';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Link } from 'react-router';
import { Button, Dialog, Position, Toaster, Intent } from "@blueprintjs/core";
import Sidebar from 'react-sidebar';

import Menu from '../components/Menu';
import WalletDetails from '../components/WalletDetails';
import DepositDialog from '../components/DepositDialog';
import WithdrawDialog from '../components/WithdrawDialog';
import db from '../services/DB';

class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDepositDialogOpen: false,
      isDepositWithdrawOpen: false,
      currentCoin: {},
      currentTransaction: ''
    };

    this.coins = {}

    console.log("CURRENT", this.props.getApp())

    for(var cc of Object.keys(coinsConfig)) {
      console.log('COIN CONFIG', coinsConfig[cc])
      this.coins[cc] = {
        wallet: WalletFactory.getInstance(cc, this.props.getApp().currentAccount.secrets, coinsConfig[cc].networks.test || coinsConfig[cc].networks.main),
        transactions: [],
        balance: "-",
        name: coinsConfig[cc].name,
        identifier: cc,
        icon: coinsConfig[cc].icon,
        ticker: 0,
      }

      break
    }

    this.state.currentCoin = this.coins[this.props.params.id]
    this.initialized = false

    // this.openSocketConnection()
    this.setupNotifications()

    this.changeCurrentCoin = this.changeCurrentCoin.bind(this)

    this.setupSocketEvents()
  }

  setupSocketEvents() {
    this.props.getApp().ws.removeAllListeners()
    var _this = this
    this.props.getApp().ws.on('ticker', function(message) {
      const coinId = message.coin.toLowerCase()
      if (_this.coins[coinId]) {
        if (_this.state.currentCoin.identifier === coinId) {
          _this.setState({
            currentCoin: Object.assign(_this.state.currentCoin, { ticker: message.price_usd })
          })
        } else {
          _this.coins[coinId].ticker = message.price_usd
        }

      }
    })
  }

  setupNotifications() {
    let _this = this;
    this.refHandlers = {
      toaster: function (ref) { return _this.toaster = ref; }
    }
  }

  openSocketConnection() {
    this.tickerSocket = io('http://socket.coincap.io');
    let _this = this;
    this.tickerSocket.on('trades', function (data) {
        const coinId = data.coin.toLowerCase()
        if (_this.coins[coinId]) {
          if (_this.state.currentCoin.identifier === coinId) {
            _this.setState({
              currentCoin: Object.assign(_this.state.currentCoin, { ticker: data.msg.price })
            })
          } else {
            _this.coins[coinId].ticker = data.msg.price
          }

        }
    })
  }

  changeCurrentCoin(coin) {
    coin = this.coins[coin]
    this.setState({
      currentCoin: coin
    })
  }

  updateBalance() {
    const successCallback = balance => {
      this.setState({
        currentCoin: Object.assign(this.state.currentCoin, { balance: balance }),
      })

      // this.props.getApp().changeApp('currentBalances', Object.assign(this.this.props.getApp().currentBalances, { [this.state.currentCoin.identifier]: balance }))
    }

    const errorCallback = error => {
      this.toaster.show({ message: error, intent: Intent.DANGER })
    }

    if (typeof this.state.currentCoin.balance === 'string')
      this.state.currentCoin.wallet.getBalance(successCallback, errorCallback)
  }

  loadTransactions() {
    const successCallback = txs => {
      this.setState({
        currentCoin: Object.assign(this.state.currentCoin, { transactions: txs })
      })
    }

    const errorCallback = error => {
      this.toaster.show({ message: error, intent: Intent.DANGER })
    }

    if (this.state.currentCoin.transactions.length === 0)
      this.state.currentCoin.wallet.getTransactions(successCallback, errorCallback)

  }

  toggleDepositDialog() {
    this.setState({ isDepositDialogOpen: !this.state.isDepositDialogOpen })
  }

  toggleWithdrawDialog() {
    this.setState({ isWithdrawDialogOpen: !this.state.isWithdrawDialogOpen })
  }

  withdraw(transaction) {
    const successCallback = tx => {
      console.log("TX STRING", JSON.stringify(tx))
      this.setState({ currentTransaction: JSON.stringify(tx) })

    }

    const errorCallback = error => {
      this.toggleWithdrawDialog()
      this.toaster.show({ message: error, intent: Intent.DANGER })
    }

    this.state.currentCoin.wallet.withdraw(transaction, successCallback, errorCallback)
  }

  doneTransaction() {
    this.setState({ currentTransaction: '' })
    this.toggleWithdrawDialog()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.id != nextProps.params.id || !this.initialized) {
      this.initialized = true
      if (nextProps.params.id == 'portfolio') {
        this.state.currentCoin = 'portfolio'
      } else {
        this.state.currentCoin = this.coins[nextProps.params.id]
        this.updateBalance()
        this.loadTransactions()
      }

    } else {
      this.state.currentCoin = nextProps.params.id == 'portfolio' ? 'portfolio' : this.coins[nextProps.params.id]
    }

  }

  render() {
    if (this.state.currentCoin == 'portfolio')
      return (
        <div>
          Portfolio render
        </div>
      )
    else
      return (
        <div className="pt-light">
          <Sidebar sidebar={<Menu selected={this.state.currentCoin.identifier} currentAccountAlias={this.props.getApp().currentAccount.alias} />}
                   docked={true}
                   shadow={false}
                   transitions={false}
                   sidebarClassName='menu'
                   contentClassName='main-content'
                   onSetOpen={this.onSetSidebarOpen}>
            <WalletDetails coin={this.state.currentCoin} onTapDepositButton={this.toggleDepositDialog.bind(this)} onTapWithdrawButton={this.toggleWithdrawDialog.bind(this)} />
          </Sidebar>

          <DepositDialog wallet={this.state.currentCoin.wallet} isOpen={this.state.isDepositDialogOpen} onToggle={this.toggleDepositDialog.bind(this)} />
          <WithdrawDialog balance={this.state.currentCoin.balance} isOpen={this.state.isWithdrawDialogOpen} onToggle={this.toggleWithdrawDialog.bind(this)} onWithdraw={this.withdraw.bind(this)} onDone={this.doneTransaction.bind(this)} currentTransaction={this.state.currentTransaction} />

          <Toaster position={Position.TOP_RIGHT} ref={this.refHandlers.toaster} />
        </div>
      )
  }
}

export default withApp(Wallet)
