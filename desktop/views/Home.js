import db from '../services/DB';
import withApp from '../withApp';
import React, { Component } from 'react';
import coins from '../coins.js'
import Bitcoin from '../coins/Bitcoin';
import bip39 from 'bip39'
import SetupSecrets from '../components/SetupSecrets';
import secret from '../services/Secret'
import { withRouter } from 'react-router';
import { Link } from 'react-router';
import { Button, Intent, Icon, Position, Toaster } from "@blueprintjs/core";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      account: {
        alias: '',
        password: '',
      },
      accounts: []
    };

    this.props.getApp().db.Accounts.all().then(result => {
      let rows = this.props.getApp().db.aggregateRows(result)
      this.setState({ accounts: rows.map((account) => account.alias) })
    })

    this.handleChange = this.handleChange.bind(this)

    this.setupNotifications()
  }

  setupNotifications() {
    let _this = this;
    this.refHandlers = {
      toaster: function (ref) { return _this.toaster = ref; }
    }
  }

  openWallet() {
    this.props.getApp().db.Accounts.where({ alias: this.state.account.alias })
      .then((result) => {
        let account = this.props.getApp().db.aggregateRows(result)[0]
        let decrypted
        try {
          decrypted = secret.decryptWithPassword(account.secrets, this.state.account.password)
          decrypted = JSON.parse(decrypted)
        } catch(e) {
          this.toaster.show({ message: "Invalid password", intent: Intent.DANGER })
          return
        }

        const mnemonic = bip39.entropyToMnemonic(decrypted.master)
        const uid = secret.generateAccountId({
          alias: this.state.account.alias,
          mnemonic: mnemonic,
          email: decrypted.email,
          pubkey1: decrypted.pubkey1,
          pubkey2: decrypted.pubkey2
        })

        this.props.getApp().ws.emit('command', { command: 'sign_in', account: { uid: uid } })
        this.props.changeApp('currentAccount', { alias: this.state.account.alias, secrets: decrypted })
        this.props.router.push('/portfolio')
      })
  }

  restoreWallet() {
    this.props.router.push('/keys/restore')
  }

  createWallet() {
    this.props.router.push('/keys/create')
  }

  handleChange(event) {
    this.setState({ account: Object.assign(this.state.account, { [event.target.name]: event.target.value }) })
  }

  render() {
    return (
      <div className="pt-light content home-content">
        <div className="wallet-icon">
          CRYPTO<br/>
          VAULT
        </div>

        <div className="pt-form-group">
          <div className="pt-form-content">
            <div className="pt-label">
              Account
              <div className="pt-select pt-fill pt-large">
                <select className="pt-large" name="alias" value={this.state.account.alias} onChange={this.handleChange}>
                  {this.state.accounts.map((account, i) => <option value={account}>{account}</option>)}
                </select>
              </div>
            </div>
          </div>
        </div>

        <div className="pt-form-group">
          <div className="pt-form-content">
            <input id="account-password" name="password" className="pt-input pt-large" style={{width: '100%'}} type="password" placeholder="Password" dir="auto" value={this.state.account.password} onChange={this.handleChange} />
          </div>
        </div>
        <Button className="pt-large" rightIconName="pt-icon-import" text="Open Wallet" onClick={this.openWallet.bind(this)} /><br /><br />

        <hr/>

        <Button className="pt-large" rightIconName="pt-icon-import" text="Restore a wallet" onClick={this.restoreWallet.bind(this)} /><br /><br />
        <Button className="pt-large" rightIconName="pt-icon-import" text="Create new wallet" onClick={this.createWallet.bind(this)} />

        <Toaster position={Position.TOP_RIGHT} ref={this.refHandlers.toaster} />
      </div>
    );
  }
}

export default withApp(Home);
