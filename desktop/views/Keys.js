import crypto from 'crypto'
import React, { Component } from 'react';
import Ripples from 'react-ripples'
import { Button, Intent, Position, Toaster } from "@blueprintjs/core";
import SetupSecrets from '../components/SetupSecrets';
import bip39 from 'bip39'
import bitcoin from 'bitcoinjs-lib'
import withApp from '../withApp';
import io from 'socket.io-client';
import secret from '../services/secret'

const masterSalt = '8f99a163-ad08-4b++CoinBundlerKey++ab-9f51-d0eec94b2c8e'
class Keys extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: this.props.params.action == 'create' ? 1 : 2,
      entropyMessage: 'Tap tap tap tap!',
      account: {
        alias: '',
        pubkey1: '',
        pubkey2: '',
        email: '',
        recoveryEmail: '',
        pin: secret.generatePIN(),
        password: ''
      },
    };

    if (this.props.params.action == 'create') {
      this.seed = new Date().toString()
      this.seedCounter = 0
    }

    this.handleChange = this.handleChange.bind(this)
    this.setupSocketEvents()
    this.setupNotifications()
  }

  setupNotifications() {
    let _this = this;
    this.refHandlers = {
      toaster: function (ref) { return _this.toaster = ref; }
    }
  }

  setupSocketEvents() {
    this.props.getApp().ws.removeAllListeners()

    var _this = this
    this.props.getApp().ws.on('response', function(message) {
      if (message.result.success) _this.props.router.push('/portfolio')

    })
  }

  goToHome() {
    this.props.router.push('/')
  }

  restore() {
    if (bip39.validateMnemonic(this.state.account.mnemonic)) {
      this.seed = bip39.mnemonicToEntropy(this.state.account.mnemonic)
      return true
    } else {
      this.toaster.show({ message: "Invalid backup phrase", intent: Intent.DANGER })
      return false
    }
  }

  showSecrets() {
    const salt = crypto.createHmac('sha512', secret.browserFingerprint(window) + new Date().toString()).update(masterSalt).digest('hex')
    this.seed = crypto.pbkdf2Sync(this.seed, salt, 1000000, 16, 'sha512')
    this.setState({
      account: Object.assign(this.state.account,
        { mnemonic: bip39.entropyToMnemonic(this.seed) })
    })
    this.setState({ step: 2 })
  }

  setupEmails() {
    if (this.props.params.action == 'restore' && !this.restore()) return
    this.setState({ step: 3 })
  }

  handleChange(event) {
    this.setState({ account: Object.assign(this.state.account, { [event.target.name]: event.target.value }) })
  }

  confirm() {
    const uid = secret.generateAccountId(this.state.account)

    let secrets = JSON.stringify({
      pubkey1: this.state.account.pubkey1,
      pubkey2: this.state.account.pubkey2,
      master: this.seed,
      email: this.state.account.email
    })

    const encryptedWithPIN = secret.encryptWithPassword(secrets, this.state.account.pin)
    const encryptedWithPassword = secret.encryptWithPassword(secrets, this.state.account.password)

    let account = {
      alias: this.state.account.alias,
      email: this.state.account.email,
      security_email: this.state.account.recoveryEmail,
      secrets: encryptedWithPIN,
      uid: uid
    }

    let _this = this
    this.props.getApp().db.Accounts.create({ alias: this.state.account.alias, secrets: encryptedWithPassword }).then(result => {
      this.seed = null
      this.props.changeApp('currentAccount', { alias: this.state.account.alias, secrets: JSON.parse(secrets) })
      if (_this.state.account.email) {
        _this.props.getApp().ws.emit('command', { command: 'sign_up', account: account })
      } else {
        _this.props.router.push('/portfolio')
      }
    })
  }

  entropy(event) {
    this.seed += event.clientX + event.clientY + new Date().getTime()
    this.seedCounter++
    if (this.seedCounter >= 10) this.showSecrets()
  }

  render() {
    if (this.state.step == 1) {
      return (
        <div className="pt-light content instructions">
          <h2>Welcome</h2>
          Annotate the backup phrase shown on next step and keep in a safe place. Anyone with these words is able to recovery your key.<br/>
          Click multiple times on button below in different positions to generate your key.<br/><br/>
          <div style={{width: '100%'}}>
            <Ripples style={{display: 'block'}}>
              <Button onClick={this.entropy.bind(this)} style={{width: '600px', height: '300px', fontSize: '20px', color: '#999'}}>{this.state.entropyMessage}</Button>
            </Ripples>
          </div>
          <br/>
        </div>
      );
    } else if (this.state.step == 2) {
      return (
        <div className="pt-light setup">
          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-mnemonic">
              Backup phrase
              <span className="pt-text-muted">(required)</span>
            </label>
            <div className="pt-form-content">
              <textarea id="account-mnemonic" readOnly={this.props.params.action == 'create'} name="mnemonic" className="pt-input pt-large" style={{width: '100%', height: 80}} type="text" dir="auto" value={this.state.account.mnemonic} onChange={this.handleChange}></textarea>
            </div>
          </div>

          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-alias">
              Account alias
              <span className="pt-text-muted">(required)</span>
            </label>
            <div className="pt-form-content">
              <input id="account-alias" name="alias" className="pt-input pt-large" style={{width: '100%'}} type="text" dir="auto" value={this.state.account.alias} onChange={this.handleChange} />
            </div>
          </div>

          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-password">
              Password
              <span className="pt-text-muted">(required)</span>
            </label>
            <div className="pt-form-content">
              <input id="account-password" name="password" className="pt-input pt-large" style={{width: '100%'}} type="password" dir="auto" value={this.state.account.password} onChange={this.handleChange} />
            </div>
          </div>

          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-pubkey1">
              Public key #1
              <span className="pt-text-muted">(required)</span>
            </label>
            <div className="pt-form-content">
              <input id="account-pubkey1" name="pubkey1" className="pt-input pt-large" style={{width: '100%'}} type="text" dir="auto" value={this.state.account.pubkey1} onChange={this.handleChange} />
            </div>
          </div>

          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-pubkey2">
              Public key #2
              <span className="pt-text-muted">(required)</span>
            </label>
            <div className="pt-form-content">
              <input id="account-pubkey2" name="pubkey2" className="pt-input pt-large" style={{width: '100%'}} type="text" dir="auto" value={this.state.account.pubkey2} onChange={this.handleChange} />
            </div>
          </div>

          <Button
              className="pt-large"
              onClick={this.goToHome.bind(this)}
              text="Cancel"
          />

          <Button
              className="pt-large"
              onClick={this.setupEmails.bind(this)}
              text="Next"
          />

          <Toaster position={Position.TOP_RIGHT} ref={this.refHandlers.toaster} />
        </div>
      );
    } else if (this.state.step == 3) {
      return (
        <div className="pt-light setup">
          Recovery your account by a third person. If you wanna skip this step leave your email blank.
          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-email">
              Your PIN
            </label>
            <div className="pt-form-content">
              {this.state.account.pin}
            </div>
          </div>

          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-email">
              Your email
              <span className="pt-text-muted">(required)</span>
            </label>
            <div className="pt-form-content">
              <input id="account-email" name="email" className="pt-input pt-large" style={{width: '100%'}} type="text" dir="auto" value={this.state.account.email} onChange={this.handleChange} />
            </div>
          </div>

          <div className="pt-form-group">
            <label className="pt-label" htmlFor="account-recovery-email">
              Recovery email
              <span className="pt-text-muted">(required)</span>
            </label>
            <div className="pt-form-content">
              <input id="account-recovery-email" name="recoveryEmail" className="pt-input pt-large" style={{width: '100%'}} type="text" dir="auto" value={this.state.account.recoveryEmail} onChange={this.handleChange} />
            </div>
          </div>

          <Button
              className="pt-large"
              onClick={this.showSecrets.bind(this)}
              text="Back"
          />
          <Button
              className="pt-large"
              onClick={this.confirm.bind(this)}
              text="Confirm"
          />
        </div>
      );
    }
  }
}

export default withApp(Keys)
