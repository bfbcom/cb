import withApp from '../withApp';
import React, { Component } from 'react';
import { Doughnut } from "react-chartjs";
import Sidebar from 'react-sidebar';
import Menu from '../components/Menu';

class Portfolio extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.portfolio = [
      {
          value: 20,
          color: "#f68914",
          highlight: "#f68914",
          label: "Bitcoin"
      },
      {
          value: 30,
          color: "#0069AD",
          highlight: "#0069AD",
          label: "Dash"
      },
      {
          value: 10,
          color: "#BA9C2D",
          highlight: "#BA9C2D",
          label: "Doge"
      },
      {
          value: 15,
          color: "#575D84",
          highlight: "#575D84",
          label: "Qtum"
      },
      {
          value: 25,
          color:"#B6B6B6",
          highlight: "#B6B6B6",
          label: "Litecoin"
      },
    ]

    console.log("APP CONFIG", this.props.getApp())
  }


  render() {
    let totals = this.portfolio.map((coin, i) =>
      <div style={{display: 'inline', margin: 20, padding: 2, paddingLeft: 8, borderWidth: 8, borderLeftStyle: 'solid', borderColor: coin.color}}>
        {coin.label} <span style={{color: '#999'}}>$ 100.0</span>
      </div>
    )

    return (
      <div className="pt-light">
        <Sidebar sidebar={<Menu selected='home' currentAccountAlias={this.props.getApp().currentAccount.alias} />}
                 docked={true}
                 shadow={false}
                 sidebarClassName='menu'
                 contentClassName='main-content'
                 transitions={false}
                 onSetOpen={this.onSetSidebarOpen}>
          <div style={{textAlign: 'center'}}>
            <Doughnut data={this.portfolio} height="250"/>
          </div>
          <div style={{textAlign: 'center', marginTop: 40}}>
            {totals}
          </div>
        </Sidebar>
      </div>
    );
  }
}

export default withApp(Portfolio)
