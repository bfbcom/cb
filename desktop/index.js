import DB from './services/DB';
import React from 'react';
import ReactDOM from 'react-dom';

import { Router, Route, hashHistory } from 'react-router'
import { AppProvider } from './components/AppProvider'
import Portfolio from './views/Portfolio';
import Wallet from './views/Wallet';
import Home from './views/Home';
import Keys from './views/Keys';
// import injectTapEventPlugin from 'react-tap-event-plugin';
// injectTapEventPlugin();

const app = document.getElementById('root');

DB.createSchema().then((results) => {
  ReactDOM.render(
    <AppProvider>
      <Router history={hashHistory}>
        <Route path="/" component={Home}/>
        <Route path="/portfolio" component={Portfolio}/>
        <Route path="/wallets/:id" component={Wallet}/>
        <Route path="/keys/:action" component={Keys}/>
      </Router>
    </AppProvider>,
    app
  );
})
