import React from 'react';
import { appContextTypes } from './components/AppProvider';

export default function withApp(WrappedComponent) {
  const Wrapper = (props, { getApp, changeApp }) => (
    <WrappedComponent
      getApp={ getApp }
      changeApp = { changeApp }
      { ...props }
    />
  );

  Wrapper.contextTypes = appContextTypes;

  return Wrapper;
}
