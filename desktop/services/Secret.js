import crypto from 'crypto'

const browserFingerprint = (w) => {
  let nav = w.navigator
  let screen = w.screen
  let id = nav.mimeTypes.length
  id += nav.userAgent.replace(/\D+/g, '')
  id += nav.plugins.length
  id += screen.height || ''
  id += screen.width || ''
  id += screen.pixelDepth || ''

  return id
}

const generatePIN = () => {
  return crypto.randomBytes(8).readUIntBE(0, 3).toString()
}

const generateAccountId = (account) => {
  const seed = [account.email, account.alias, account.pubkey1,
    account.pubkey2, account.mnemonic.split(' ').pop()]
  return crypto.createHash('sha256').update(seed.join('+')).digest('hex')
}

const derivateKey = (passphrase) => {
  const masterSalt = "8981bec1-4f7a-4e++CoinBundlerPassword++e7-9853-1c66581aa5e1"
  const salt = crypto.createHmac('sha512', passphrase).update(masterSalt).digest('hex')
  return crypto.pbkdf2Sync(passphrase,
    salt, 100000, 64, 'sha512')
}

const encryptWithPassword = (message, password) => {
  const pw = derivateKey(password)

  const cipher = crypto.createCipher('aes256', pw)
  let encrypted = cipher.update(message, 'utf8', 'hex')
  encrypted += cipher.final('hex')

  return encrypted
}

const decryptWithPassword = (encrypted, password) => {
  const pw = derivateKey(password)

  const decipher = crypto.createDecipher('aes256', pw)
  let decrypted = decipher.update(encrypted, 'hex', 'utf8')
  decrypted += decipher.final('utf8')

  return decrypted
}

const Secret = {
  browserFingerprint: browserFingerprint,
  generateAccountId: generateAccountId,
  generatePIN: generatePIN,
  // encryptWithPIN: encryptWithPIN,
  encryptWithPassword: encryptWithPassword,
  decryptWithPassword: decryptWithPassword
}

export default Secret
