import websql from 'websql-promisified';

const database = openDatabase('wallet_db', '1.0', 'Wallet Database', 2 * 1024 * 1024);
const websqlPromise = websql(database);

const createSchema = () => {
  return websqlPromise.transaction((tx) => {
    // tx.executeSql('CREATE TABLE IF NOT EXISTS wallets (coin, balance REAL, publicAddress)')
    // tx.executeSql('CREATE TABLE IF NOT EXISTS transactions (coin, amount INTEGER, addressFrom, addressTo, txid, date DATETIME, operation, coin, accountId)')
    tx.executeSql('CREATE TABLE IF NOT EXISTS accounts (alias, secrets)')
    tx.executeSql('CREATE TABLE IF NOT EXISTS coins (name, price)')
    tx.executeSql('CREATE TABLE IF NOT EXISTS accounts_coins (accountId INTEGER, coinId INTEGER, balance, FOREIGN KEY(accountId) REFERENCES accounts(rowid), FOREIGN KEY(coinId) REFERENCES coins(rowid))')
  })
}

const aggregateRows = (result) => {
  let records = []
  for(let i=0; i< result[0].rows.length; i++) {
      let row = result[0].rows.item(i)
      records.push(row)
   }

   return records
}

const test = () => {
  console.log("TEST LOAD DB")
}

const db = {
  insert: (table, object) =>
    websqlPromise.transaction((tx) => {
      tx.executeSql("INSERT INTO " + table + " (" + Object.keys(object).join(",") + ") VALUES ('" + Object.values(object).join("','") + "')")
    }),
  find: (table, id) =>
    websqlPromise.transaction((tx) => {
      tx.executeSql("SELECT rowid AS id,* FROM " + table + " WHERE rowid = '" + value + "'")
    }),
  where: (table, conditions) =>
    websqlPromise.transaction((tx) => {
      var query = conditions
      if (conditions && typeof(conditions) === 'object') {
        var numColumns = Object.keys(conditions).length,
            addedColumns = 0

        query = ""

        for (let [key, value] of Object.entries(conditions)) {
          addedColumns++
          query += key + " = '" + value + "'"
          if (addedColumns < numColumns) query += " AND "
        }
      } else {
        query = "1 = 1"
      }
      tx.executeSql("SELECT rowid AS id,* FROM " + table + " WHERE " + query)
    })
};

const Accounts = {
  create: account =>
    db.insert('accounts', account),
  where: conditions =>
    db.where('accounts', conditions),
  all: () =>
    db.where('accounts')
}

const Transactions = {
  create: transaction =>
    db.insert('transactions', transaction),
  where: conditions =>
    db.where('transactions', conditions)
};

const Config = {
  set: (key, value) =>
    window.localStorage.setItem(key, value),
  get: (key) =>
    window.localStorage.getItem(key)
}

const Wallets = {
  create: wallet =>
    db.insert('wallets', wallet),
  find: coin =>
    db.where('wallets', { coin: coin })
};

export default {
  Accounts,
  Transactions,
  Wallets,
  createSchema,
  test,
  aggregateRows,
  Config
};
