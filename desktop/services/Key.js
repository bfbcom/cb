import libcoin from 'bitcoinjs-lib'

class Key {
  constructor(master, pubkey1, pubkey2, network) {
    this._network = network
    // derivação inicial da chave privada da carteira, obtêm-se um ramo capaz de derivar novas chaves filhas
    this._master = libcoin.HDNode.fromSeedHex(master, network).derivePath("m/46'/" + this._network.bip44)
    // configuração das chaves públicas no formato de exportação já inicialmente derivadas
    this._pubkey1 = libcoin.HDNode.fromBase58(pubkey1, network).derivePath(this._network.bip44)
    this._pubkey2 = libcoin.HDNode.fromBase58(pubkey2, network).derivePath(this._network.bip44)
    // obtenção do endereço principal da carteira
    this._publicAddress = this.getNewPublicAddress()
    // chave privada responsável pela assinatura nessa aplicação, derivada com base no padrão definido
    this._privateKey = this._master.derivePath("0/0").keyPair
  }

  getPublicAddress() {
    return this._publicAddress.toString()
  }

  getPublicKeys(path = '0/0') {
    // retorna as chaves públicas derivados por um mesmo ramo/caminho
    return [this._master.derivePath(path).getPublicKeyBuffer(),
      this._pubkey1.derivePath(path).getPublicKeyBuffer(),
      this._pubkey2.derivePath(path).getPublicKeyBuffer()]
  }

  getRedeemScript() {
    // obtém o unlock script, sendo necessário duas das três chaves privadas envolvidas para desbloquear
    // 2 PK1 PK2 PK3 3 OP_CHECKMULTISIG
    return libcoin.script.multisig.output.encode(2, this.getPublicKeys())
  }

  getNewPublicAddress() {
    // criação do script e transformação do hash
    const scriptPubKey = libcoin.script.scriptHash.output.encode(libcoin.crypto.hash160(this.getRedeemScript()))
    // encoding do hash do script com os parâmetros da criptomoeda para obter o endereço público de recebimento
    const address = libcoin.address.fromOutputScript(scriptPubKey, this._network)
    return address
  }

}

export default Key
