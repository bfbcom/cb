import rp from 'request-promise';

class Blockchain {
  constructor(network) {
    this.network = network
  }

  balance(address) {
    return this._getRequest(this.network.api.addressBalance.replace('{{address}}', address))
  }

  unspent(address) {
    return this._getRequest(this.network.api.addressUtxo.replace('{{address}}', address))
  }

  fee() {
    return this._getRequest(this.network.api.fee)
  }

  txs(address) {
    return this._getRequest(this.network.api.addressTxs.replace('{{address}}', address))
  }

  broadcastTx(rawTx) {
    return this._postRequest(this.network.api.broadcastTx, { rawtx: rawTx })
  }

  _getRequest(uri) {
    return rp({ uri: uri, json: true })
  }

  _postRequest(uri, data) {
    return rp({ uri: uri, method: 'post', body: data, json: true })
  }
}

export default Blockchain
