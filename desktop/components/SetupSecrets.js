import QRCode from 'qrcode.react'
import React, { Component } from 'react';
import { Button, Intent } from "@blueprintjs/core";

class SetupSecrets extends Component {
  constructor(props) {
    super(props);
    this.goToWallet = this.goToWallet.bind(this)
  }

  goToWallet() {
    this.props.router.push('/wallet')
  }

  render() {
    return (
      <div>
        <div className="setup-secret">
          <div className="key-qrcode">
            <QRCode value={this.props.shares[0]} />
          </div>
          <div className="key-text">
            <h5>Key 1</h5>
            {this.props.shares[0]}
          </div>
        </div>
        <div className="setup-secret">
          <div className="key-qrcode">
            <QRCode value={this.props.shares[1]} />
          </div>
          <div className="key-text">
            <h5>Key 2</h5>
            {this.props.shares[1]}
          </div>
        </div>
        <div className="setup-secret">
          <div className="key-qrcode">
            <QRCode value={this.props.shares[2]} />
          </div>
          <div className="key-text">
            <h5>Key 3</h5>
            {this.props.shares[2]}
          </div>
        </div>
        <Button className="pt-large" iconName="print" text="Print" />
        <Button className="pt-large" rightIconName="pt-icon-arrow-right" text="Open my wallet" onClick={this.goToWallet} />
      </div>
    );
  }
}

export default SetupSecrets;
