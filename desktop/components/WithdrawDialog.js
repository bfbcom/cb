import QRCode from 'qrcode.react'
import React, { Component } from 'react'
import { Menu, MenuItem, MenuDivider, Button, Dialog } from "@blueprintjs/core"
import { IntlProvider, FormattedDate } from 'react-intl'

class WithdrawDialog extends Component {
  constructor(props) {
    super(props);

    this.toggleWithdrawDialog = this.toggleWithdrawDialog.bind(this)
    this.confirmWithdraw = this.confirmWithdraw.bind(this)

    this.state = {
      transaction: {
        amount: 0.0,
        address: 'mgbArhRA79u95iuBuK5rH6WyHZwX8t6cjW',
        fee: 0.1,
        string: '',
        raw: ''
      },
    }

    this.handleChange = this.handleChange.bind(this)
  }

  toggleWithdrawDialog() {
    this.props.onToggle(!this.props.isOpen)
  }

  confirmWithdraw() {
    this.props.onWithdraw(this.state.transaction)
  }

  handleChange(event) {
    this.setState({ transaction: Object.assign(this.state.transaction, { [event.target.name]: event.target.value }) })
  }

  done() {
    this.props.onDone()
  }

  render() {
    if (this.props.currentTransaction)
      return (
        <Dialog
            iconName="arrow-top-right"
            isOpen={this.props.isOpen}
            onClose={this.toggleWithdrawDialog}
            title="Withdraw"
        >
          <div className="pt-dialog-body transaction-qr-code">
            <QRCode value={this.props.currentTransaction} size={200} /><br/><br/>
            Scan this QR Code with your signer app to finish the transaction.
          </div>
          <div className="pt-dialog-footer">

            <div className="pt-dialog-footer-actions">
                <Button
                    onClick={this.done.bind(this)}
                    text="Done"
                />
            </div>
          </div>
        </Dialog>
      )
    else
      return (
        <Dialog
            iconName="arrow-top-right"
            isOpen={this.props.isOpen}
            onClose={this.toggleWithdrawDialog}
            title="Withdraw"
        >
            <div className="pt-dialog-body">
              <div className="pt-form-group">
                <label className="pt-label" htmlFor="transaction-address">
                  To address
                  <span className="pt-text-muted">(required)</span>
                </label>
                <div className="pt-form-content">
                  <input id="transaction-address" name="address" className="pt-input pt-large" style={{width: '100%'}} placeholder="" type="text" dir="auto" value={this.state.transaction.address} onChange={this.handleChange} />
                  <div className="pt-form-helper-text">Inform the correct address to transfer the funds.</div>
                </div>
              </div>

              <div className="pt-form-group">
                <label className="pt-label" htmlFor="transaction-amount">
                  Amount
                  <span className="pt-text-muted">(required)</span>
                </label>
                <div className="pt-form-content">
                  <input id="transaction-amount" name="amount" className="pt-input pt-large" style={{width: '100%'}} placeholder="0.001" type="text" dir="auto" value={this.state.transaction.amount} onChange={this.handleChange} />
                  <div className="pt-form-helper-text">Balance: {this.props.balance}</div>
                </div>
              </div>

            </div>
            <div className="pt-dialog-footer">
                <div className="pt-dialog-footer-actions">
                    <Button
                        onClick={this.confirmWithdraw}
                        text="Withdraw"
                    />
                </div>
            </div>
        </Dialog>
      );
  }
}

export default WithdrawDialog;
