const QRCode = require('qrcode.react')

import React, { Component } from 'react';
import { Menu, MenuItem, MenuDivider, Button, Dialog } from "@blueprintjs/core";
import { IntlProvider, FormattedDate } from 'react-intl';

class DepositDialog extends Component {
  constructor(props) {
    super(props);

    this.toggleDepositDialog = this.toggleDepositDialog.bind(this)
  }

  toggleDepositDialog() {
    this.props.onToggle(!this.props.isOpen)
  }

  render() {
    return (
      <Dialog
          iconName="arrow-bottom-right"
          isOpen={this.props.isOpen}
          onClose={this.toggleDepositDialog}
          title="Deposit"
      >
          <div className="pt-dialog-body deposit-dialog">
            Deposit your funds to this address.<br/><br/>
           <QRCode value={this.props.wallet.getPublicAddress()} /><br/><br/>
           <input className="pt-input pt-large pt-fill" type="text" dir="auto" value={this.props.wallet.getPublicAddress()} />
          </div>
      </Dialog>
    );
  }
}

export default DepositDialog;
