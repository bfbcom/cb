import React, { Component } from 'react';
import { IntlProvider, FormattedTime } from 'react-intl';
import { Button } from "@blueprintjs/core";

const {shell} = require('electron')


class Transaction extends Component {
  constructor(props) {
    super(props);
  }

  openTx() {
    shell.openExternal(this.props.transaction.url);
  }

  render() {
    return (
      <tr>
        <td>
          <FormattedTime
          value={new Date(this.props.transaction.time)}
          day="numeric"
          month="long"
          year="numeric" />
        </td>
        <td style={{textAlign: 'right'}}>{this.props.transaction.operation === 'C' ? '+' : '-'}{this.props.transaction.amount}</td>
        <td width="20px"><Button className="pt-small" iconName="share" onClick={this.openTx.bind(this)} /></td>
      </tr>
    );
  }
}

export default Transaction;
