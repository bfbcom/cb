import React, { Component } from 'react';
import { Link } from 'react-router';
import * as coins from '../coins';

class Menu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let options = Object.keys(coins).map((coin, i) =>
      <li><Link className={this.props.selected === coin ? 'selected' : ''} to={"/wallets/" + coin}>{coins[coin].name}</Link></li>
    );

    return (
      <div>
        <div className="wallet-icon">
          CRYPTO<br/>
          VAULT
          <p>
            {this.props.currentAccountAlias}
          </p>
        </div>

        <ul>
          <li><Link className={this.props.selected === 'home' ? 'selected' : ''} to="/wallets/portfolio">Portfolio</Link></li>
          <li className="divider">Wallets</li>
          {options}
        </ul>
      </div>
    );
  }
}

export default Menu;
