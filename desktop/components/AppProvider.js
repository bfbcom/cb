import io from 'socket.io-client';
import React from 'react';
import propTypes from 'prop-types';
import db from '../services/DB';

export const appContextTypes = {
  getApp: propTypes.func,
  changeApp: propTypes.func
};

export class AppProvider extends React.Component {
  state = {
    // ws: io('https://localhost:3005', { secure: true, reconnect: true, rejectUnauthorized: false }),
    ws: io('http://localhost:3005'),
    db: db,
    currentAccount: {},
    currentBalances: {}
  }

  static childContextTypes = appContextTypes;

  getChildContext() {
    return {
      getApp: () => this.state,
      changeApp: (attr, value) => this.setState({ [attr]: value })
    }
  }

  render() {
    return <span>{ this.props.children }</span>
  }
};
