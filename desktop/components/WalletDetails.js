import React, { Component } from 'react';
import { Menu, MenuItem, MenuDivider, Button, Dialog } from "@blueprintjs/core";
import { IntlProvider, FormattedDate } from 'react-intl';
import Transaction from './Transaction'

class WalletDetails extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let transactions = this.props.coin.transactions.map((tx, i) => <Transaction transaction={tx} />);
    return (
      <IntlProvider locale="en">
        <div className="wallet-details">
          <img src={this.props.coin.icon} />
          <div className="balance">
            <span>Balance</span>
            {this.props.coin.balance}
            <div className="fiat">
              $ {(this.props.coin.balance === '-' ? 0.0 : this.props.coin.balance * this.props.coin.ticker).toFixed(2)}
            </div>
          </div>

          <Button className="pt-large bt-action" text="Deposit" iconName="arrow-bottom-right" onClick={this.props.onTapDepositButton} />
          <Button className="pt-large bt-action" text="Withdraw" rightIconName="arrow-top-right" onClick={this.props.onTapWithdrawButton} />

          <div className="transactions">
            <h5>Transactions</h5>
            <table className="pt-table" width="100%">
              <tbody>
                {transactions}
              </tbody>
            </table>
          </div>
        </div>
      </IntlProvider>
    );
  }
}

export default WalletDetails;
