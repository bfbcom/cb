import Blockchain from '../services/Blockchain'
import Key from '../services/Key'
import _ from 'lodash'
import coinSelect from 'coinselect'
import libcoin from 'bitcoinjs-lib'

class Qtum extends Key {
  constructor(secrets, network) {
    super(secrets.master, secrets.pubkey1, secrets.pubkey2, network)
    this.blockchain = new Blockchain(network)
    this._network = network
  }

  getBalance(successCallback, errorCallback) {
    this.blockchain.balance(this.getPublicAddress()).then(data => {
      console.log('success balance', data)
      successCallback(Number.parseFloat(data.balance))
    }).catch(error => {
      console.log("ERROR", error)
      errorCallback("QTUM: Error on sync blockchain")
    })
  }

  withdraw(txData, successCallback, errorCallback) {
    let _this = this
    this.blockchain.unspent(this.getPublicAddress()).then(utxos => {
      utxos = utxos.map(utxo => { return {
        txId: utxo.txid,
        vout: utxo.vout,
        value: utxo.satoshis,
      } })

      let pv = _this._privateKey
      let redeemScript = _this.getRedeemScript()
      let targets = [{ address: txData.address, value: Number.parseInt(Number.parseFloat(txData.amount) * 100000000)}]
      //let fee = get fee estimation = (0,00015061/1024)*100000000 // satoshis per byte
      let { inputs, outputs, fee } = coinSelect(utxos, targets, 1000)

      if (!inputs || !outputs) return

      let txb = new libcoin.TransactionBuilder(_this._network)

      inputs.forEach(input => {
        txb.addInput(input.txId, input.vout)
      })
      outputs.forEach(output => {
        // // watch out, outputs may have been added that you need to provide
        // // an output address/script for
        if (!output.address) {
          output.address = this.getPublicAddress()
        }

        txb.addOutput(output.address, output.value)
      })

      inputs.forEach((input, index) => {
        txb.sign(index, pv, redeemScript)
      })

      let tx = txb.build()
      successCallback({ txraw: tx.toHex(), coin: 'qtum', network: _this._network.name, redeemScript: redeemScript.toString('hex'), amount: txData.amount, to: txData.address })
    })
  }

  getTransactions(successCallback, errorCallback) {
    let txs = []
    const address = this.getPublicAddress()
    this.blockchain.txs(address).then(data => {
      data.txs.map(tx => {
        const operation = _.filter(tx.vin, { 'addr': address }).length > 0 ? 'D' : 'C'
        let amount = 0
        if (operation == 'D') {
          _.filter(tx.vout, (vout) => {
            return !_.includes(vout.scriptPubKey.addresses, address)
          }).forEach(vout => { amount += Number.parseFloat(vout.value) })
        } else {
          _.filter(tx.vout, (vout) => {
            return _.includes(vout.scriptPubKey.addresses, address)
          }).forEach(vout => { amount += Number.parseFloat(vout.value) })
        }

        const addressTo = _.filter(tx.vout, (vout) => {
          return _.includes(vout.scriptPubKey.addresses, address)
        }).map(vout => { vout.scriptPubKey.addresses.first })

        txs.push({
          txid: tx.txid,
          operation: operation,
          address: operation == 'D' ? addressTo : null,
          time: tx.time*1000,
          amount: amount,
          url: this._network.txUrl.replace('{{txId}}', tx.txid)
        })
      })

      successCallback(txs)
    }).catch(error => {
      console.log(error)
      errorCallback("QTUM: Error on sync blockchain")
    })
  }

}

export default Qtum;
