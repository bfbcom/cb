import Bitcoin from './Bitcoin'
import Dash from './Dash'
import Decred from './Decred'
import Qtum from './Qtum'
// import Doge from './Doge'
import Groestlcoin from './Groestlcoin'
import Litecoin from './Litecoin'
import Vertcoin from './Vertcoin'

class WalletFactory {
  static getInstance(coin, secrets, network) {
    switch (coin) {
      case "btc":
        return new Bitcoin(secrets, network)
      case "dash":
        return new Dash(secrets, network)
      case "dcr":
        return new Decred(secrets, network)
      case "qtum":
        return new Qtum(secrets, network)
      // case "doge":
      //   return new Doge(key)
      // case "eth":
      //   return new Ethereum(key)
      case "grs":
        return new Groestlcoin(secrets, network)
      case "ltc":
        return new Litecoin(secrets, network)
      case "vtc":
        return new Vertcoin(secrets, network)
      default:
        throw new Error(`Could not instantiate wallet for ${coin}`);
    }
  }
}

export default WalletFactory
