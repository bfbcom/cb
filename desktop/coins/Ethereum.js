import _ from 'lodash'
import ethereum from 'ethereumjs-util'
import EthereumTx from 'ethereumjs-tx'
import Web3 from 'web3'
import { eth } from '../coins'

class Ethereum {
  constructor(bytes) {
    this.privateKey = new Buffer(bytes, 'hex')
    this.blockchain = new Web3(new Web3.providers.HttpProvider(eth.networks.ropsten.providerUri));
  }

  getPublicAddress() {
    return ethereum.bufferToHex(ethereum.privateToAddress(this.privateKey))
  }

  getBalance(successCallback, errorCallback) {
    this.blockchain.eth.getBalance(this.getPublicAddress(), (error, result) => {
      if (error) {
        errorCallback("ETH: Error on sync blockchain")
      } else {
        successCallback(this.blockchain.fromWei(result.toNumber(), 'ether'))
      }
    })

  }

  withdraw(tx, successCallback, errorCallback) {
    console.log("Tx", tx)
    var transaction = new EthereumTx(null, 3) // mainnet Tx EIP155
    transaction.nonce = 0
    // transaction.gasPrice = 1
    // transaction.gasLimit = 1000
    transaction.value = this.blockchain.toWei(Number.parseFloat(tx.amount), 'ether')
    transaction.to = tx.address
    transaction.sign(this.privateKey)
    // var fee = transaction.getUpfrontCost()
    transaction.gas = 40000
    // console.log("GAS", fee.toString())
    let serialized = ethereum.addHexPrefix(transaction.serialize().toString('hex'))
    this.blockchain.eth.sendRawTransaction(serialized, (error, hash) => {
      console.log(error, hash)
      successCallback(tx)
    })
  }

  getTransactions(successCallback, errorCallback) {

  }

}

export default Ethereum;
