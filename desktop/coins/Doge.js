import { doge } from '../coins'
import Blockchain from '../services/Blockchain'
import Key from '../services/Key'
import _ from 'lodash'
import bitcore from 'bitcore-lib'

bitcore.Networks.add({
  name: 'dogetestnet',
  alias: 'dogetestnet',
  pubkeyhash: doge.networks.test.versions.public,
  privatekey: doge.networks.test.versions.private,
  scripthash: doge.networks.test.versions.scripthash,
  xpubkey: null,
  xprivkey: null
})

class Doge extends Key {
  constructor(bytes) {
    super(bytes, doge.networks.test.versions)
    this.blockchain = new Blockchain('DOGETEST')
  }

  getBalance(successCallback, errorCallback) {
    this.blockchain.balance(this.getPublicAddress()).then(data => {
      if (data.status === 'success') {
        successCallback(Number.parseFloat(data.data.confirmed_balance))
      } else {
        errorCallback("DOGE: Error on sync blockchain")
      }
    }).catch(error => {
      errorCallback("DOGE: Error on sync blockchain")
    })
  }

  withdraw(tx, successCallback, errorCallback) {
    // build transaction and broadcast
    let _this = this
    this.blockchain.unspent(this.getPublicAddress()).then(data => {
      var utxos = data.data.txs.map((tx) => {
        return {
          txId: tx.txid,
          satoshis: Number.parseInt(Number.parseFloat(tx.value) * 100000000),
          outputIndex: tx.output_no,
          address: data.address,
          script: tx.script_hex,
        }
      })

      let pv = bitcore.PrivateKey.fromWIF(_this.getPrivateWif())
      var transaction = new bitcore.Transaction()
      transaction.from(utxos)
      transaction.to(tx.address, parseFloat(tx.amount) * 100000000)
      transaction.change(_this.getPublicAddress())
      transaction.sign(pv)
      _this.blockchain.broadcastTx(transaction.serialize()).then(data => {
        if (data.status === 'success') {
          successCallback(tx)
        } else {
          errorCallback('Error on broadcast transaction')
        }
      }).catch(error => {
        errorCallback('Error on broadcast transaction')
      })
    })
  }

  getTransactions(successCallback, errorCallback) {
    let _this = this
    let txs = []
    this.blockchain.receivedTxs(this.getPublicAddress()).then(data => {
      txs = txs.concat(data.data.txs)
      _this.blockchain.spentTxs(this.getPublicAddress()).then(data => {
        if (data.status === 'success') {
          txs = txs.concat(data.data.txs)
          successCallback(_this.formatTxs(txs))
        }
      })
    }).catch(error => {
      console.log(error)
      errorCallback("DOGE: Error on sync blockchain")
    })
  }

  formatTxs(txs) {
    var ftxs = []

    for(var tx of txs) {
      ftxs.push({
        txid: tx.txid,
        operation: tx.hasOwnProperty('output_no') ? 'C' : 'D',
        amount: tx.value,
        confirmations: tx.confirmations,
        time: tx.time,
        url: doge.networks.test.txUrl.replace("{{txId}}", tx.txid)
      })
    }

    return ftxs;
  }

}

export default Doge;
